/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mokshamanukantha
 */
@Embeddable
public class ReservationSlotPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "slot_id", nullable = false)
    private int slotId;
    @Basic(optional = false)
    @Column(name = "spot_id", nullable = false)
    private int spotId;
    @Basic(optional = false)
    @Column(name = "reservation_item_id", nullable = false)
    private int reservationItemId;
    @Basic(optional = false)
    @Column(name = "campaign_id", nullable = false)
    private int campaignId;

    public ReservationSlotPK() {
    }

    public ReservationSlotPK(int slotId, int spotId, int reservationItemId, int campaignId) {
        this.slotId = slotId;
        this.spotId = spotId;
        this.reservationItemId = reservationItemId;
        this.campaignId = campaignId;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public int getSpotId() {
        return spotId;
    }

    public void setSpotId(int spotId) {
        this.spotId = spotId;
    }

    public int getReservationItemId() {
        return reservationItemId;
    }

    public void setReservationItemId(int reservationItemId) {
        this.reservationItemId = reservationItemId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) slotId;
        hash += (int) spotId;
        hash += (int) reservationItemId;
        hash += (int) campaignId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationSlotPK)) {
            return false;
        }
        ReservationSlotPK other = (ReservationSlotPK) object;
        if (this.slotId != other.slotId) {
            return false;
        }
        if (this.spotId != other.spotId) {
            return false;
        }
        if (this.reservationItemId != other.reservationItemId) {
            return false;
        }
        if (this.campaignId != other.campaignId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationSlotPK[ slotId=" + slotId + ", spotId=" + spotId + ", reservationItemId=" + reservationItemId + ", campaignId=" + campaignId + " ]";
    }
    
}
