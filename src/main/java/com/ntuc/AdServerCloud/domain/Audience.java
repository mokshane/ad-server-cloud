/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Audience.findAll", query = "SELECT a FROM Audience a")})
public class Audience implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "audience_id", nullable = false)
    private Integer audienceId;
    @Column(name = "audience_name", length = 200)
    private String audienceName;
    @Column(name = "adest_id")
    private Integer adestId;
    @ManyToMany(mappedBy = "audienceList", fetch = FetchType.LAZY)
    private List<Adset> adsetList;

    public Audience() {
    }

    public Audience(Integer audienceId) {
        this.audienceId = audienceId;
    }

    public Integer getAudienceId() {
        return audienceId;
    }

    public void setAudienceId(Integer audienceId) {
        this.audienceId = audienceId;
    }

    public String getAudienceName() {
        return audienceName;
    }

    public void setAudienceName(String audienceName) {
        this.audienceName = audienceName;
    }

    public Integer getAdestId() {
        return adestId;
    }

    public void setAdestId(Integer adestId) {
        this.adestId = adestId;
    }

    public List<Adset> getAdsetList() {
        return adsetList;
    }

    public void setAdsetList(List<Adset> adsetList) {
        this.adsetList = adsetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (audienceId != null ? audienceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Audience)) {
            return false;
        }
        Audience other = (Audience) object;
        if ((this.audienceId == null && other.audienceId != null) || (this.audienceId != null && !this.audienceId.equals(other.audienceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.Audience[ audienceId=" + audienceId + " ]";
    }
    
}
