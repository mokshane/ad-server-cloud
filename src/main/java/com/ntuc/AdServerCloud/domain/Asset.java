/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Asset.findAll", query = "SELECT a FROM Asset a")})
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "asset_id", nullable = false)
    private Integer assetId;
    @Basic(optional = false)
    @Column(name = "owner_bu", nullable = false, length = 200)
    private String ownerBu;
    @Basic(optional = false)
    @Column(name = "asset_name", nullable = false, length = 200)
    private String assetName;
    @JoinTable(name = "asset_store", joinColumns = {
        @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "store_id", referencedColumnName = "store_id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Store> storeList;
    @ManyToMany(mappedBy = "assetList", fetch = FetchType.LAZY)
    private List<Adset> adsetList;
    @JoinColumn(name = "asset_type_id", referencedColumnName = "asset_type_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AssetType assetTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asset", fetch = FetchType.LAZY)
    private List<AssetPackageItem> assetPackageItemList;

    public Asset() {
    }

    public Asset(Integer assetId) {
        this.assetId = assetId;
    }

    public Asset(Integer assetId, String ownerBu, String assetName) {
        this.assetId = assetId;
        this.ownerBu = ownerBu;
        this.assetName = assetName;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public String getOwnerBu() {
        return ownerBu;
    }

    public void setOwnerBu(String ownerBu) {
        this.ownerBu = ownerBu;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }

    public List<Adset> getAdsetList() {
        return adsetList;
    }

    public void setAdsetList(List<Adset> adsetList) {
        this.adsetList = adsetList;
    }

    public AssetType getAssetTypeId() {
        return assetTypeId;
    }

    public void setAssetTypeId(AssetType assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public List<AssetPackageItem> getAssetPackageItemList() {
        return assetPackageItemList;
    }

    public void setAssetPackageItemList(List<AssetPackageItem> assetPackageItemList) {
        this.assetPackageItemList = assetPackageItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assetId != null ? assetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asset)) {
            return false;
        }
        Asset other = (Asset) object;
        if ((this.assetId == null && other.assetId != null) || (this.assetId != null && !this.assetId.equals(other.assetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.Asset[ assetId=" + assetId + " ]";
    }
    
}
