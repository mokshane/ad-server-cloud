/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mokshamanukantha
 */
@Embeddable
public class ReservationPackageItemPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "reservation_item_id", nullable = false)
    private int reservationItemId;
    @Basic(optional = false)
    @Column(name = "campaign_id", nullable = false)
    private int campaignId;
    @Basic(optional = false)
    @Column(name = "package_id", nullable = false)
    private int packageId;
    @Basic(optional = false)
    @Column(name = "package_item_id", nullable = false)
    private int packageItemId;

    public ReservationPackageItemPK() {
    }

    public ReservationPackageItemPK(int reservationItemId, int campaignId, int packageId, int packageItemId) {
        this.reservationItemId = reservationItemId;
        this.campaignId = campaignId;
        this.packageId = packageId;
        this.packageItemId = packageItemId;
    }

    public int getReservationItemId() {
        return reservationItemId;
    }

    public void setReservationItemId(int reservationItemId) {
        this.reservationItemId = reservationItemId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getPackageItemId() {
        return packageItemId;
    }

    public void setPackageItemId(int packageItemId) {
        this.packageItemId = packageItemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) reservationItemId;
        hash += (int) campaignId;
        hash += (int) packageId;
        hash += (int) packageItemId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationPackageItemPK)) {
            return false;
        }
        ReservationPackageItemPK other = (ReservationPackageItemPK) object;
        if (this.reservationItemId != other.reservationItemId) {
            return false;
        }
        if (this.campaignId != other.campaignId) {
            return false;
        }
        if (this.packageId != other.packageId) {
            return false;
        }
        if (this.packageItemId != other.packageItemId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationPackageItemPK[ reservationItemId=" + reservationItemId + ", campaignId=" + campaignId + ", packageId=" + packageId + ", packageItemId=" + packageItemId + " ]";
    }
    
}
