/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "asset_package_item", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "AssetPackageItem.findAll", query = "SELECT a FROM AssetPackageItem a")})
public class AssetPackageItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AssetPackageItemPK assetPackageItemPK;
    @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Asset asset;
    @JoinColumn(name = "package_id", referencedColumnName = "package_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MediaPackage mediaPackage;
    @JoinColumn(name = "package_item_id", referencedColumnName = "package_item_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MediaPackageItem mediaPackageItem;

    public AssetPackageItem() {
    }

    public AssetPackageItem(AssetPackageItemPK assetPackageItemPK) {
        this.assetPackageItemPK = assetPackageItemPK;
    }

    public AssetPackageItem(int assetId, int packageId, int packageItemId) {
        this.assetPackageItemPK = new AssetPackageItemPK(assetId, packageId, packageItemId);
    }

    public AssetPackageItemPK getAssetPackageItemPK() {
        return assetPackageItemPK;
    }

    public void setAssetPackageItemPK(AssetPackageItemPK assetPackageItemPK) {
        this.assetPackageItemPK = assetPackageItemPK;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public MediaPackage getMediaPackage() {
        return mediaPackage;
    }

    public void setMediaPackage(MediaPackage mediaPackage) {
        this.mediaPackage = mediaPackage;
    }

    public MediaPackageItem getMediaPackageItem() {
        return mediaPackageItem;
    }

    public void setMediaPackageItem(MediaPackageItem mediaPackageItem) {
        this.mediaPackageItem = mediaPackageItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assetPackageItemPK != null ? assetPackageItemPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssetPackageItem)) {
            return false;
        }
        AssetPackageItem other = (AssetPackageItem) object;
        if ((this.assetPackageItemPK == null && other.assetPackageItemPK != null) || (this.assetPackageItemPK != null && !this.assetPackageItemPK.equals(other.assetPackageItemPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.AssetPackageItem[ assetPackageItemPK=" + assetPackageItemPK + " ]";
    }
    
}
