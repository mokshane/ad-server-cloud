/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "campaign_reservation", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "CampaignReservation.findAll", query = "SELECT c FROM CampaignReservation c")})
public class CampaignReservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "campaign_id", nullable = false)
    private Integer campaignId;
    @Basic(optional = false)
    @Column(name = "campaign_name", nullable = false, length = 200)
    private String campaignName;
    @Basic(optional = false)
    @Column(name = "campaign_start_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date campaignStartDate;
    @Column(name = "campaign_duration")
    private Integer campaignDuration;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "campaign_budget", precision = 22, scale = 0)
    private Double campaignBudget;
    @Column(name = "campaign_objective", length = 200)
    private String campaignObjective;
    @JoinTable(name = "reservation_package", joinColumns = {
        @JoinColumn(name = "campaign_id", referencedColumnName = "campaign_id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "campaign_id", referencedColumnName = "package_id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<MediaPackage> mediaPackageList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campaignReservation", fetch = FetchType.LAZY)
    private List<ReservationDailyAsset> reservationDailyAssetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campaignReservation", fetch = FetchType.LAZY)
    private List<ReservationSlot> reservationSlotList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campaignReservation", fetch = FetchType.LAZY)
    private List<ReservationPackageItem> reservationPackageItemList;
    @JoinColumn(name = "advertiser_id", referencedColumnName = "advertiser_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AdvertiserClient advertiserId;
    @JoinColumn(name = "agent_id", referencedColumnName = "agent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ResellAgent agentId;
    @OneToMany(mappedBy = "campaignId", fetch = FetchType.LAZY)
    private List<ReservationSpot> reservationSpotList;

    public CampaignReservation() {
    }

    public CampaignReservation(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public CampaignReservation(Integer campaignId, String campaignName, Date campaignStartDate) {
        this.campaignId = campaignId;
        this.campaignName = campaignName;
        this.campaignStartDate = campaignStartDate;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Date getCampaignStartDate() {
        return campaignStartDate;
    }

    public void setCampaignStartDate(Date campaignStartDate) {
        this.campaignStartDate = campaignStartDate;
    }

    public Integer getCampaignDuration() {
        return campaignDuration;
    }

    public void setCampaignDuration(Integer campaignDuration) {
        this.campaignDuration = campaignDuration;
    }

    public Double getCampaignBudget() {
        return campaignBudget;
    }

    public void setCampaignBudget(Double campaignBudget) {
        this.campaignBudget = campaignBudget;
    }

    public String getCampaignObjective() {
        return campaignObjective;
    }

    public void setCampaignObjective(String campaignObjective) {
        this.campaignObjective = campaignObjective;
    }

    public List<MediaPackage> getMediaPackageList() {
        return mediaPackageList;
    }

    public void setMediaPackageList(List<MediaPackage> mediaPackageList) {
        this.mediaPackageList = mediaPackageList;
    }

    public List<ReservationDailyAsset> getReservationDailyAssetList() {
        return reservationDailyAssetList;
    }

    public void setReservationDailyAssetList(List<ReservationDailyAsset> reservationDailyAssetList) {
        this.reservationDailyAssetList = reservationDailyAssetList;
    }

    public List<ReservationSlot> getReservationSlotList() {
        return reservationSlotList;
    }

    public void setReservationSlotList(List<ReservationSlot> reservationSlotList) {
        this.reservationSlotList = reservationSlotList;
    }

    public List<ReservationPackageItem> getReservationPackageItemList() {
        return reservationPackageItemList;
    }

    public void setReservationPackageItemList(List<ReservationPackageItem> reservationPackageItemList) {
        this.reservationPackageItemList = reservationPackageItemList;
    }

    public AdvertiserClient getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(AdvertiserClient advertiserId) {
        this.advertiserId = advertiserId;
    }

    public ResellAgent getAgentId() {
        return agentId;
    }

    public void setAgentId(ResellAgent agentId) {
        this.agentId = agentId;
    }

    public List<ReservationSpot> getReservationSpotList() {
        return reservationSpotList;
    }

    public void setReservationSpotList(List<ReservationSpot> reservationSpotList) {
        this.reservationSpotList = reservationSpotList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campaignId != null ? campaignId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampaignReservation)) {
            return false;
        }
        CampaignReservation other = (CampaignReservation) object;
        if ((this.campaignId == null && other.campaignId != null) || (this.campaignId != null && !this.campaignId.equals(other.campaignId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.CampaignReservation[ campaignId=" + campaignId + " ]";
    }
    
}
