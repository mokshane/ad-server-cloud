/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "resell_agent", catalog = "db", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"agent_name"})})
@NamedQueries({
    @NamedQuery(name = "ResellAgent.findAll", query = "SELECT r FROM ResellAgent r")})
public class ResellAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "agent_id", nullable = false)
    private Integer agentId;
    @Basic(optional = false)
    @Column(name = "agent_name", nullable = false, length = 20)
    private String agentName;
    @Lob
    @Column(name = "agent_revenue_share", length = 16777215)
    private String agentRevenueShare;
    @Lob
    @Column(name = "advertiser_website_url", length = 2147483647)
    private String advertiserWebsiteUrl;
    @OneToMany(mappedBy = "agentId", fetch = FetchType.LAZY)
    private List<CampaignReservation> campaignReservationList;

    public ResellAgent() {
    }

    public ResellAgent(Integer agentId) {
        this.agentId = agentId;
    }

    public ResellAgent(Integer agentId, String agentName) {
        this.agentId = agentId;
        this.agentName = agentName;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentRevenueShare() {
        return agentRevenueShare;
    }

    public void setAgentRevenueShare(String agentRevenueShare) {
        this.agentRevenueShare = agentRevenueShare;
    }

    public String getAdvertiserWebsiteUrl() {
        return advertiserWebsiteUrl;
    }

    public void setAdvertiserWebsiteUrl(String advertiserWebsiteUrl) {
        this.advertiserWebsiteUrl = advertiserWebsiteUrl;
    }

    public List<CampaignReservation> getCampaignReservationList() {
        return campaignReservationList;
    }

    public void setCampaignReservationList(List<CampaignReservation> campaignReservationList) {
        this.campaignReservationList = campaignReservationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agentId != null ? agentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResellAgent)) {
            return false;
        }
        ResellAgent other = (ResellAgent) object;
        if ((this.agentId == null && other.agentId != null) || (this.agentId != null && !this.agentId.equals(other.agentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ResellAgent[ agentId=" + agentId + " ]";
    }
    
}
