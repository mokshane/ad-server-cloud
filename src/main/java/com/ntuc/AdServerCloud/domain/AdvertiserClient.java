/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "advertiser_client", catalog = "db", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"advertiser_name"})})
@NamedQueries({
    @NamedQuery(name = "AdvertiserClient.findAll", query = "SELECT a FROM AdvertiserClient a")})
public class AdvertiserClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "advertiser_id", nullable = false)
    private Integer advertiserId;
    @Basic(optional = false)
    @Column(name = "advertiser_name", nullable = false, length = 20)
    private String advertiserName;
    @Lob
    @Column(name = "advertiser_website_url", length = 2147483647)
    private String advertiserWebsiteUrl;
    @OneToMany(mappedBy = "advertiserId", fetch = FetchType.LAZY)
    private List<CampaignReservation> campaignReservationList;

    public AdvertiserClient() {
    }

    public AdvertiserClient(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }

    public AdvertiserClient(Integer advertiserId, String advertiserName) {
        this.advertiserId = advertiserId;
        this.advertiserName = advertiserName;
    }

    public Integer getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getAdvertiserName() {
        return advertiserName;
    }

    public void setAdvertiserName(String advertiserName) {
        this.advertiserName = advertiserName;
    }

    public String getAdvertiserWebsiteUrl() {
        return advertiserWebsiteUrl;
    }

    public void setAdvertiserWebsiteUrl(String advertiserWebsiteUrl) {
        this.advertiserWebsiteUrl = advertiserWebsiteUrl;
    }

    public List<CampaignReservation> getCampaignReservationList() {
        return campaignReservationList;
    }

    public void setCampaignReservationList(List<CampaignReservation> campaignReservationList) {
        this.campaignReservationList = campaignReservationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (advertiserId != null ? advertiserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdvertiserClient)) {
            return false;
        }
        AdvertiserClient other = (AdvertiserClient) object;
        if ((this.advertiserId == null && other.advertiserId != null) || (this.advertiserId != null && !this.advertiserId.equals(other.advertiserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.AdvertiserClient[ advertiserId=" + advertiserId + " ]";
    }
    
}
