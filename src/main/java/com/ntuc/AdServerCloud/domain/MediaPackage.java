/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "media_package", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "MediaPackage.findAll", query = "SELECT m FROM MediaPackage m")})
public class MediaPackage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "package_id", nullable = false)
    private Integer packageId;
    @Basic(optional = false)
    @Column(name = "package_name", nullable = false, length = 500)
    private String packageName;
    @Column(name = "package_description", length = 1000)
    private String packageDescription;
    @Column(name = "package_duration")
    private Integer packageDuration;
    @Basic(optional = false)
    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "expire_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date expireDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "package_price", precision = 22, scale = 0)
    private Double packagePrice;
    @ManyToMany(mappedBy = "mediaPackageList", fetch = FetchType.LAZY)
    private List<CampaignReservation> campaignReservationList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mediaPackage", fetch = FetchType.LAZY)
    private MediaPackageItem mediaPackageItem;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mediaPackage", fetch = FetchType.LAZY)
    private List<ReservationPackageItem> reservationPackageItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mediaPackage", fetch = FetchType.LAZY)
    private List<AssetPackageItem> assetPackageItemList;

    public MediaPackage() {
    }

    public MediaPackage(Integer packageId) {
        this.packageId = packageId;
    }

    public MediaPackage(Integer packageId, String packageName, Date creationDate, Date expireDate) {
        this.packageId = packageId;
        this.packageName = packageName;
        this.creationDate = creationDate;
        this.expireDate = expireDate;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public Integer getPackageDuration() {
        return packageDuration;
    }

    public void setPackageDuration(Integer packageDuration) {
        this.packageDuration = packageDuration;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(Double packagePrice) {
        this.packagePrice = packagePrice;
    }

    public List<CampaignReservation> getCampaignReservationList() {
        return campaignReservationList;
    }

    public void setCampaignReservationList(List<CampaignReservation> campaignReservationList) {
        this.campaignReservationList = campaignReservationList;
    }

    public MediaPackageItem getMediaPackageItem() {
        return mediaPackageItem;
    }

    public void setMediaPackageItem(MediaPackageItem mediaPackageItem) {
        this.mediaPackageItem = mediaPackageItem;
    }

    public List<ReservationPackageItem> getReservationPackageItemList() {
        return reservationPackageItemList;
    }

    public void setReservationPackageItemList(List<ReservationPackageItem> reservationPackageItemList) {
        this.reservationPackageItemList = reservationPackageItemList;
    }

    public List<AssetPackageItem> getAssetPackageItemList() {
        return assetPackageItemList;
    }

    public void setAssetPackageItemList(List<AssetPackageItem> assetPackageItemList) {
        this.assetPackageItemList = assetPackageItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (packageId != null ? packageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediaPackage)) {
            return false;
        }
        MediaPackage other = (MediaPackage) object;
        if ((this.packageId == null && other.packageId != null) || (this.packageId != null && !this.packageId.equals(other.packageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.MediaPackage[ packageId=" + packageId + " ]";
    }
    
}
