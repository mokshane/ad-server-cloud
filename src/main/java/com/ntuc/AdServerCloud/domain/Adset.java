/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Adset.findAll", query = "SELECT a FROM Adset a")})
public class Adset implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "adset_id", nullable = false)
    private Integer adsetId;
    @Basic(optional = false)
    @Column(name = "adset_name", nullable = false, length = 200)
    private String adsetName;
    @Column(name = "adset_duration")
    private Integer adsetDuration;
    @JoinTable(name = "adset_audience", joinColumns = {
        @JoinColumn(name = "adset_id", referencedColumnName = "adset_id")}, inverseJoinColumns = {
        @JoinColumn(name = "audience_id", referencedColumnName = "audience_id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Audience> audienceList;
    @JoinTable(name = "asset_adset", joinColumns = {
        @JoinColumn(name = "adset_id", referencedColumnName = "adset_id", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Asset> assetList;
    @OneToMany(mappedBy = "adsetId", fetch = FetchType.LAZY)
    private List<Playlist> playlistList;

    public Adset() {
    }

    public Adset(Integer adsetId) {
        this.adsetId = adsetId;
    }

    public Adset(Integer adsetId, String adsetName) {
        this.adsetId = adsetId;
        this.adsetName = adsetName;
    }

    public Integer getAdsetId() {
        return adsetId;
    }

    public void setAdsetId(Integer adsetId) {
        this.adsetId = adsetId;
    }

    public String getAdsetName() {
        return adsetName;
    }

    public void setAdsetName(String adsetName) {
        this.adsetName = adsetName;
    }

    public Integer getAdsetDuration() {
        return adsetDuration;
    }

    public void setAdsetDuration(Integer adsetDuration) {
        this.adsetDuration = adsetDuration;
    }

    public List<Audience> getAudienceList() {
        return audienceList;
    }

    public void setAudienceList(List<Audience> audienceList) {
        this.audienceList = audienceList;
    }

    public List<Asset> getAssetList() {
        return assetList;
    }

    public void setAssetList(List<Asset> assetList) {
        this.assetList = assetList;
    }

    public List<Playlist> getPlaylistList() {
        return playlistList;
    }

    public void setPlaylistList(List<Playlist> playlistList) {
        this.playlistList = playlistList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adsetId != null ? adsetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adset)) {
            return false;
        }
        Adset other = (Adset) object;
        if ((this.adsetId == null && other.adsetId != null) || (this.adsetId != null && !this.adsetId.equals(other.adsetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.Adset[ adsetId=" + adsetId + " ]";
    }
    
}
