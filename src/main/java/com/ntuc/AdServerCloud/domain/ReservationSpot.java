/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "reservation_spot", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "ReservationSpot.findAll", query = "SELECT r FROM ReservationSpot r")})
public class ReservationSpot implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReservationSpotPK reservationSpotPK;
    @Column(name = "adset_id")
    private Integer adsetId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    @Column(name = "asset_id")
    private Integer assetId;
    @Column(name = "remaining_slots")
    private Integer remainingSlots;
    @Column(name = "total_slot")
    private Integer totalSlot;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservationSpot", fetch = FetchType.LAZY)
    private List<ReservationSlot> reservationSlotList;
    @JoinColumn(name = "campaign_id", referencedColumnName = "campaign_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CampaignReservation campaignId;
    @JoinColumn(name = "reservation_item_id", referencedColumnName = "reservation_item_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ReservationPackageItem reservationPackageItem;

    public ReservationSpot() {
    }

    public ReservationSpot(ReservationSpotPK reservationSpotPK) {
        this.reservationSpotPK = reservationSpotPK;
    }

    public ReservationSpot(int spotId, int reservationItemId) {
        this.reservationSpotPK = new ReservationSpotPK(spotId, reservationItemId);
    }

    public ReservationSpotPK getReservationSpotPK() {
        return reservationSpotPK;
    }

    public void setReservationSpotPK(ReservationSpotPK reservationSpotPK) {
        this.reservationSpotPK = reservationSpotPK;
    }

    public Integer getAdsetId() {
        return adsetId;
    }

    public void setAdsetId(Integer adsetId) {
        this.adsetId = adsetId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public Integer getRemainingSlots() {
        return remainingSlots;
    }

    public void setRemainingSlots(Integer remainingSlots) {
        this.remainingSlots = remainingSlots;
    }

    public Integer getTotalSlot() {
        return totalSlot;
    }

    public void setTotalSlot(Integer totalSlot) {
        this.totalSlot = totalSlot;
    }

    public List<ReservationSlot> getReservationSlotList() {
        return reservationSlotList;
    }

    public void setReservationSlotList(List<ReservationSlot> reservationSlotList) {
        this.reservationSlotList = reservationSlotList;
    }

    public CampaignReservation getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(CampaignReservation campaignId) {
        this.campaignId = campaignId;
    }

    public ReservationPackageItem getReservationPackageItem() {
        return reservationPackageItem;
    }

    public void setReservationPackageItem(ReservationPackageItem reservationPackageItem) {
        this.reservationPackageItem = reservationPackageItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationSpotPK != null ? reservationSpotPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationSpot)) {
            return false;
        }
        ReservationSpot other = (ReservationSpot) object;
        if ((this.reservationSpotPK == null && other.reservationSpotPK != null) || (this.reservationSpotPK != null && !this.reservationSpotPK.equals(other.reservationSpotPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationSpot[ reservationSpotPK=" + reservationSpotPK + " ]";
    }
    
}
