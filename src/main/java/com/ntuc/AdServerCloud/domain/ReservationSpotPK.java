/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mokshamanukantha
 */
@Embeddable
public class ReservationSpotPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "spot_id", nullable = false)
    private int spotId;
    @Basic(optional = false)
    @Column(name = "reservation_item_id", nullable = false)
    private int reservationItemId;

    public ReservationSpotPK() {
    }

    public ReservationSpotPK(int spotId, int reservationItemId) {
        this.spotId = spotId;
        this.reservationItemId = reservationItemId;
    }

    public int getSpotId() {
        return spotId;
    }

    public void setSpotId(int spotId) {
        this.spotId = spotId;
    }

    public int getReservationItemId() {
        return reservationItemId;
    }

    public void setReservationItemId(int reservationItemId) {
        this.reservationItemId = reservationItemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) spotId;
        hash += (int) reservationItemId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationSpotPK)) {
            return false;
        }
        ReservationSpotPK other = (ReservationSpotPK) object;
        if (this.spotId != other.spotId) {
            return false;
        }
        if (this.reservationItemId != other.reservationItemId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationSpotPK[ spotId=" + spotId + ", reservationItemId=" + reservationItemId + " ]";
    }
    
}
