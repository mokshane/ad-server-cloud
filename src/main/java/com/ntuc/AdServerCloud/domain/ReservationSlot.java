/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "reservation_slot", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "ReservationSlot.findAll", query = "SELECT r FROM ReservationSlot r")})
public class ReservationSlot implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReservationSlotPK reservationSlotPK;
    @Column(name = "asset_id")
    private Integer assetId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    @Column(name = "slot_duration")
    private Integer slotDuration;
    @JoinColumn(name = "campaign_id", referencedColumnName = "campaign_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CampaignReservation campaignReservation;
    @JoinColumn(name = "reservation_item_id", referencedColumnName = "reservation_item_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ReservationPackageItem reservationPackageItem;
    @JoinColumn(name = "spot_id", referencedColumnName = "spot_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ReservationSpot reservationSpot;

    public ReservationSlot() {
    }

    public ReservationSlot(ReservationSlotPK reservationSlotPK) {
        this.reservationSlotPK = reservationSlotPK;
    }

    public ReservationSlot(int slotId, int spotId, int reservationItemId, int campaignId) {
        this.reservationSlotPK = new ReservationSlotPK(slotId, spotId, reservationItemId, campaignId);
    }

    public ReservationSlotPK getReservationSlotPK() {
        return reservationSlotPK;
    }

    public void setReservationSlotPK(ReservationSlotPK reservationSlotPK) {
        this.reservationSlotPK = reservationSlotPK;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(Integer slotDuration) {
        this.slotDuration = slotDuration;
    }

    public CampaignReservation getCampaignReservation() {
        return campaignReservation;
    }

    public void setCampaignReservation(CampaignReservation campaignReservation) {
        this.campaignReservation = campaignReservation;
    }

    public ReservationPackageItem getReservationPackageItem() {
        return reservationPackageItem;
    }

    public void setReservationPackageItem(ReservationPackageItem reservationPackageItem) {
        this.reservationPackageItem = reservationPackageItem;
    }

    public ReservationSpot getReservationSpot() {
        return reservationSpot;
    }

    public void setReservationSpot(ReservationSpot reservationSpot) {
        this.reservationSpot = reservationSpot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationSlotPK != null ? reservationSlotPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationSlot)) {
            return false;
        }
        ReservationSlot other = (ReservationSlot) object;
        if ((this.reservationSlotPK == null && other.reservationSlotPK != null) || (this.reservationSlotPK != null && !this.reservationSlotPK.equals(other.reservationSlotPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationSlot[ reservationSlotPK=" + reservationSlotPK + " ]";
    }
    
}
