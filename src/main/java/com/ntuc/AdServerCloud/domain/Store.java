/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(catalog = "db", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"store_name"})})
@NamedQueries({
    @NamedQuery(name = "Store.findAll", query = "SELECT s FROM Store s")})
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "store_id", nullable = false)
    private Integer storeId;
    @Basic(optional = false)
    @Column(name = "store_name", nullable = false, length = 500)
    private String storeName;
    @Column(name = "store_open")
    @Temporal(TemporalType.TIMESTAMP)
    private Date storeOpen;
    @Column(name = "store_closed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date storeClosed;
    @ManyToMany(mappedBy = "storeList", fetch = FetchType.LAZY)
    private List<Asset> assetList;
    @JoinColumn(name = "store_location_id", referencedColumnName = "store_location_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreLocation storeLocationId;

    public Store() {
    }

    public Store(Integer storeId) {
        this.storeId = storeId;
    }

    public Store(Integer storeId, String storeName) {
        this.storeId = storeId;
        this.storeName = storeName;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Date getStoreOpen() {
        return storeOpen;
    }

    public void setStoreOpen(Date storeOpen) {
        this.storeOpen = storeOpen;
    }

    public Date getStoreClosed() {
        return storeClosed;
    }

    public void setStoreClosed(Date storeClosed) {
        this.storeClosed = storeClosed;
    }

    public List<Asset> getAssetList() {
        return assetList;
    }

    public void setAssetList(List<Asset> assetList) {
        this.assetList = assetList;
    }

    public StoreLocation getStoreLocationId() {
        return storeLocationId;
    }

    public void setStoreLocationId(StoreLocation storeLocationId) {
        this.storeLocationId = storeLocationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (storeId != null ? storeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Store)) {
            return false;
        }
        Store other = (Store) object;
        if ((this.storeId == null && other.storeId != null) || (this.storeId != null && !this.storeId.equals(other.storeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.Store[ storeId=" + storeId + " ]";
    }
    
}
