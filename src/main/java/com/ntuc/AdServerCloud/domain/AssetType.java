/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "asset_type", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "AssetType.findAll", query = "SELECT a FROM AssetType a")})
public class AssetType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "asset_type_id", nullable = false)
    private Integer assetTypeId;
    @Basic(optional = false)
    @Column(name = "asset_type", nullable = false, length = 200)
    private String assetType;
    @OneToMany(mappedBy = "assetTypeId", fetch = FetchType.LAZY)
    private List<Asset> assetList;

    public AssetType() {
    }

    public AssetType(Integer assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public AssetType(Integer assetTypeId, String assetType) {
        this.assetTypeId = assetTypeId;
        this.assetType = assetType;
    }

    public Integer getAssetTypeId() {
        return assetTypeId;
    }

    public void setAssetTypeId(Integer assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public List<Asset> getAssetList() {
        return assetList;
    }

    public void setAssetList(List<Asset> assetList) {
        this.assetList = assetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assetTypeId != null ? assetTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssetType)) {
            return false;
        }
        AssetType other = (AssetType) object;
        if ((this.assetTypeId == null && other.assetTypeId != null) || (this.assetTypeId != null && !this.assetTypeId.equals(other.assetTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.AssetType[ assetTypeId=" + assetTypeId + " ]";
    }
    
}
