/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "store_location", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "StoreLocation.findAll", query = "SELECT s FROM StoreLocation s")})
public class StoreLocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "store_location_id", nullable = false)
    private Integer storeLocationId;
    @Basic(optional = false)
    @Column(name = "store_geo_tag", nullable = false, length = 200)
    private String storeGeoTag;
    @Basic(optional = false)
    @Column(name = "store_no", nullable = false, length = 20)
    private String storeNo;
    @Basic(optional = false)
    @Column(name = "store_street_address", nullable = false, length = 200)
    private String storeStreetAddress;
    @Basic(optional = false)
    @Column(name = "store_town", nullable = false, length = 200)
    private String storeTown;
    @Basic(optional = false)
    @Column(name = "store_postcode", nullable = false)
    private int storePostcode;
    @Basic(optional = false)
    @Column(name = "store_mail", nullable = false, length = 500)
    private String storeMail;
    @Basic(optional = false)
    @Column(name = "store_number", nullable = false)
    private int storeNumber;
    @OneToMany(mappedBy = "storeLocationId", fetch = FetchType.LAZY)
    private List<Store> storeList;

    public StoreLocation() {
    }

    public StoreLocation(Integer storeLocationId) {
        this.storeLocationId = storeLocationId;
    }

    public StoreLocation(Integer storeLocationId, String storeGeoTag, String storeNo, String storeStreetAddress, String storeTown, int storePostcode, String storeMail, int storeNumber) {
        this.storeLocationId = storeLocationId;
        this.storeGeoTag = storeGeoTag;
        this.storeNo = storeNo;
        this.storeStreetAddress = storeStreetAddress;
        this.storeTown = storeTown;
        this.storePostcode = storePostcode;
        this.storeMail = storeMail;
        this.storeNumber = storeNumber;
    }

    public Integer getStoreLocationId() {
        return storeLocationId;
    }

    public void setStoreLocationId(Integer storeLocationId) {
        this.storeLocationId = storeLocationId;
    }

    public String getStoreGeoTag() {
        return storeGeoTag;
    }

    public void setStoreGeoTag(String storeGeoTag) {
        this.storeGeoTag = storeGeoTag;
    }

    public String getStoreNo() {
        return storeNo;
    }

    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public String getStoreStreetAddress() {
        return storeStreetAddress;
    }

    public void setStoreStreetAddress(String storeStreetAddress) {
        this.storeStreetAddress = storeStreetAddress;
    }

    public String getStoreTown() {
        return storeTown;
    }

    public void setStoreTown(String storeTown) {
        this.storeTown = storeTown;
    }

    public int getStorePostcode() {
        return storePostcode;
    }

    public void setStorePostcode(int storePostcode) {
        this.storePostcode = storePostcode;
    }

    public String getStoreMail() {
        return storeMail;
    }

    public void setStoreMail(String storeMail) {
        this.storeMail = storeMail;
    }

    public int getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(int storeNumber) {
        this.storeNumber = storeNumber;
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (storeLocationId != null ? storeLocationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StoreLocation)) {
            return false;
        }
        StoreLocation other = (StoreLocation) object;
        if ((this.storeLocationId == null && other.storeLocationId != null) || (this.storeLocationId != null && !this.storeLocationId.equals(other.storeLocationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.StoreLocation[ storeLocationId=" + storeLocationId + " ]";
    }
    
}
