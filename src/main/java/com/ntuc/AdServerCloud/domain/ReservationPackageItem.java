/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "reservation_package_item", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "ReservationPackageItem.findAll", query = "SELECT r FROM ReservationPackageItem r")})
public class ReservationPackageItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReservationPackageItemPK reservationPackageItemPK;
    @Column(name = "total_duration")
    private Integer totalDuration;
    @Column(name = "number_of_spots_booked")
    private Integer numberOfSpotsBooked;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(precision = 22, scale = 0)
    private Double amount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservationPackageItem", fetch = FetchType.LAZY)
    private List<ReservationDailyAsset> reservationDailyAssetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservationPackageItem", fetch = FetchType.LAZY)
    private List<ReservationSlot> reservationSlotList;
    @JoinColumn(name = "campaign_id", referencedColumnName = "campaign_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CampaignReservation campaignReservation;
    @JoinColumn(name = "campaign_id", referencedColumnName = "package_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MediaPackage mediaPackage;
    @JoinColumn(name = "package_item_id", referencedColumnName = "package_item_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MediaPackageItem mediaPackageItem;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservationPackageItem", fetch = FetchType.LAZY)
    private List<ReservationSpot> reservationSpotList;

    public ReservationPackageItem() {
    }

    public ReservationPackageItem(ReservationPackageItemPK reservationPackageItemPK) {
        this.reservationPackageItemPK = reservationPackageItemPK;
    }

    public ReservationPackageItem(int reservationItemId, int campaignId, int packageId, int packageItemId) {
        this.reservationPackageItemPK = new ReservationPackageItemPK(reservationItemId, campaignId, packageId, packageItemId);
    }

    public ReservationPackageItemPK getReservationPackageItemPK() {
        return reservationPackageItemPK;
    }

    public void setReservationPackageItemPK(ReservationPackageItemPK reservationPackageItemPK) {
        this.reservationPackageItemPK = reservationPackageItemPK;
    }

    public Integer getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(Integer totalDuration) {
        this.totalDuration = totalDuration;
    }

    public Integer getNumberOfSpotsBooked() {
        return numberOfSpotsBooked;
    }

    public void setNumberOfSpotsBooked(Integer numberOfSpotsBooked) {
        this.numberOfSpotsBooked = numberOfSpotsBooked;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<ReservationDailyAsset> getReservationDailyAssetList() {
        return reservationDailyAssetList;
    }

    public void setReservationDailyAssetList(List<ReservationDailyAsset> reservationDailyAssetList) {
        this.reservationDailyAssetList = reservationDailyAssetList;
    }

    public List<ReservationSlot> getReservationSlotList() {
        return reservationSlotList;
    }

    public void setReservationSlotList(List<ReservationSlot> reservationSlotList) {
        this.reservationSlotList = reservationSlotList;
    }

    public CampaignReservation getCampaignReservation() {
        return campaignReservation;
    }

    public void setCampaignReservation(CampaignReservation campaignReservation) {
        this.campaignReservation = campaignReservation;
    }

    public MediaPackage getMediaPackage() {
        return mediaPackage;
    }

    public void setMediaPackage(MediaPackage mediaPackage) {
        this.mediaPackage = mediaPackage;
    }

    public MediaPackageItem getMediaPackageItem() {
        return mediaPackageItem;
    }

    public void setMediaPackageItem(MediaPackageItem mediaPackageItem) {
        this.mediaPackageItem = mediaPackageItem;
    }

    public List<ReservationSpot> getReservationSpotList() {
        return reservationSpotList;
    }

    public void setReservationSpotList(List<ReservationSpot> reservationSpotList) {
        this.reservationSpotList = reservationSpotList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationPackageItemPK != null ? reservationPackageItemPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationPackageItem)) {
            return false;
        }
        ReservationPackageItem other = (ReservationPackageItem) object;
        if ((this.reservationPackageItemPK == null && other.reservationPackageItemPK != null) || (this.reservationPackageItemPK != null && !this.reservationPackageItemPK.equals(other.reservationPackageItemPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationPackageItem[ reservationPackageItemPK=" + reservationPackageItemPK + " ]";
    }
    
}
