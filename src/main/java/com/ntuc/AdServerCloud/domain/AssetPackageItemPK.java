/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mokshamanukantha
 */
@Embeddable
public class AssetPackageItemPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "asset_id", nullable = false)
    private int assetId;
    @Basic(optional = false)
    @Column(name = "package_id", nullable = false)
    private int packageId;
    @Basic(optional = false)
    @Column(name = "package_item_id", nullable = false)
    private int packageItemId;

    public AssetPackageItemPK() {
    }

    public AssetPackageItemPK(int assetId, int packageId, int packageItemId) {
        this.assetId = assetId;
        this.packageId = packageId;
        this.packageItemId = packageItemId;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getPackageItemId() {
        return packageItemId;
    }

    public void setPackageItemId(int packageItemId) {
        this.packageItemId = packageItemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) assetId;
        hash += (int) packageId;
        hash += (int) packageItemId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssetPackageItemPK)) {
            return false;
        }
        AssetPackageItemPK other = (AssetPackageItemPK) object;
        if (this.assetId != other.assetId) {
            return false;
        }
        if (this.packageId != other.packageId) {
            return false;
        }
        if (this.packageItemId != other.packageItemId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.AssetPackageItemPK[ assetId=" + assetId + ", packageId=" + packageId + ", packageItemId=" + packageItemId + " ]";
    }
    
}
