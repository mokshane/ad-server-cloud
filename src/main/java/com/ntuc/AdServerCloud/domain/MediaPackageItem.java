/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "media_package_item", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "MediaPackageItem.findAll", query = "SELECT m FROM MediaPackageItem m")})
public class MediaPackageItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "package_item_id", nullable = false)
    private Integer packageItemId;
    @Basic(optional = false)
    @Column(name = "package_item_name", nullable = false, length = 500)
    private String packageItemName;
    @Column(name = "package_item_description", length = 1000)
    private String packageItemDescription;
    @Column(name = "package_item_duration")
    private Integer packageItemDuration;
    @Basic(optional = false)
    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "expire_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date expireDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "package_item_price", precision = 22, scale = 0)
    private Double packageItemPrice;
    @Column(name = "package_asset_type_id")
    private Integer packageAssetTypeId;
    @JoinColumn(name = "package_item_id", referencedColumnName = "package_id", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private MediaPackage mediaPackage;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mediaPackageItem", fetch = FetchType.LAZY)
    private List<ReservationPackageItem> reservationPackageItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mediaPackageItem", fetch = FetchType.LAZY)
    private List<AssetPackageItem> assetPackageItemList;

    public MediaPackageItem() {
    }

    public MediaPackageItem(Integer packageItemId) {
        this.packageItemId = packageItemId;
    }

    public MediaPackageItem(Integer packageItemId, String packageItemName, Date creationDate, Date expireDate) {
        this.packageItemId = packageItemId;
        this.packageItemName = packageItemName;
        this.creationDate = creationDate;
        this.expireDate = expireDate;
    }

    public Integer getPackageItemId() {
        return packageItemId;
    }

    public void setPackageItemId(Integer packageItemId) {
        this.packageItemId = packageItemId;
    }

    public String getPackageItemName() {
        return packageItemName;
    }

    public void setPackageItemName(String packageItemName) {
        this.packageItemName = packageItemName;
    }

    public String getPackageItemDescription() {
        return packageItemDescription;
    }

    public void setPackageItemDescription(String packageItemDescription) {
        this.packageItemDescription = packageItemDescription;
    }

    public Integer getPackageItemDuration() {
        return packageItemDuration;
    }

    public void setPackageItemDuration(Integer packageItemDuration) {
        this.packageItemDuration = packageItemDuration;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Double getPackageItemPrice() {
        return packageItemPrice;
    }

    public void setPackageItemPrice(Double packageItemPrice) {
        this.packageItemPrice = packageItemPrice;
    }

    public Integer getPackageAssetTypeId() {
        return packageAssetTypeId;
    }

    public void setPackageAssetTypeId(Integer packageAssetTypeId) {
        this.packageAssetTypeId = packageAssetTypeId;
    }

    public MediaPackage getMediaPackage() {
        return mediaPackage;
    }

    public void setMediaPackage(MediaPackage mediaPackage) {
        this.mediaPackage = mediaPackage;
    }

    public List<ReservationPackageItem> getReservationPackageItemList() {
        return reservationPackageItemList;
    }

    public void setReservationPackageItemList(List<ReservationPackageItem> reservationPackageItemList) {
        this.reservationPackageItemList = reservationPackageItemList;
    }

    public List<AssetPackageItem> getAssetPackageItemList() {
        return assetPackageItemList;
    }

    public void setAssetPackageItemList(List<AssetPackageItem> assetPackageItemList) {
        this.assetPackageItemList = assetPackageItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (packageItemId != null ? packageItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediaPackageItem)) {
            return false;
        }
        MediaPackageItem other = (MediaPackageItem) object;
        if ((this.packageItemId == null && other.packageItemId != null) || (this.packageItemId != null && !this.packageItemId.equals(other.packageItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.MediaPackageItem[ packageItemId=" + packageItemId + " ]";
    }
    
}
