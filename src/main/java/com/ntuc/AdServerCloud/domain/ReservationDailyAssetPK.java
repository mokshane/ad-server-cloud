/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mokshamanukantha
 */
@Embeddable
public class ReservationDailyAssetPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "daily_asset_id", nullable = false)
    private int dailyAssetId;
    @Basic(optional = false)
    @Column(name = "campaign_id", nullable = false)
    private int campaignId;
    @Basic(optional = false)
    @Column(name = "reservation_item_id", nullable = false)
    private int reservationItemId;

    public ReservationDailyAssetPK() {
    }

    public ReservationDailyAssetPK(int dailyAssetId, int campaignId, int reservationItemId) {
        this.dailyAssetId = dailyAssetId;
        this.campaignId = campaignId;
        this.reservationItemId = reservationItemId;
    }

    public int getDailyAssetId() {
        return dailyAssetId;
    }

    public void setDailyAssetId(int dailyAssetId) {
        this.dailyAssetId = dailyAssetId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public int getReservationItemId() {
        return reservationItemId;
    }

    public void setReservationItemId(int reservationItemId) {
        this.reservationItemId = reservationItemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) dailyAssetId;
        hash += (int) campaignId;
        hash += (int) reservationItemId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationDailyAssetPK)) {
            return false;
        }
        ReservationDailyAssetPK other = (ReservationDailyAssetPK) object;
        if (this.dailyAssetId != other.dailyAssetId) {
            return false;
        }
        if (this.campaignId != other.campaignId) {
            return false;
        }
        if (this.reservationItemId != other.reservationItemId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationDailyAssetPK[ dailyAssetId=" + dailyAssetId + ", campaignId=" + campaignId + ", reservationItemId=" + reservationItemId + " ]";
    }
    
}
