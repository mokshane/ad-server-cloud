/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ntuc.AdServerCloud.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mokshamanukantha
 */
@Entity
@Table(name = "reservation_daily_asset", catalog = "db", schema = "")
@NamedQueries({
    @NamedQuery(name = "ReservationDailyAsset.findAll", query = "SELECT r FROM ReservationDailyAsset r")})
public class ReservationDailyAsset implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReservationDailyAssetPK reservationDailyAssetPK;
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    @Column(name = "remaining_slots")
    private Integer remainingSlots;
    @Column(name = "total_slot")
    private Integer totalSlot;
    @JoinColumn(name = "campaign_id", referencedColumnName = "campaign_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CampaignReservation campaignReservation;
    @JoinColumn(name = "reservation_item_id", referencedColumnName = "reservation_item_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ReservationPackageItem reservationPackageItem;

    public ReservationDailyAsset() {
    }

    public ReservationDailyAsset(ReservationDailyAssetPK reservationDailyAssetPK) {
        this.reservationDailyAssetPK = reservationDailyAssetPK;
    }

    public ReservationDailyAsset(int dailyAssetId, int campaignId, int reservationItemId) {
        this.reservationDailyAssetPK = new ReservationDailyAssetPK(dailyAssetId, campaignId, reservationItemId);
    }

    public ReservationDailyAssetPK getReservationDailyAssetPK() {
        return reservationDailyAssetPK;
    }

    public void setReservationDailyAssetPK(ReservationDailyAssetPK reservationDailyAssetPK) {
        this.reservationDailyAssetPK = reservationDailyAssetPK;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getRemainingSlots() {
        return remainingSlots;
    }

    public void setRemainingSlots(Integer remainingSlots) {
        this.remainingSlots = remainingSlots;
    }

    public Integer getTotalSlot() {
        return totalSlot;
    }

    public void setTotalSlot(Integer totalSlot) {
        this.totalSlot = totalSlot;
    }

    public CampaignReservation getCampaignReservation() {
        return campaignReservation;
    }

    public void setCampaignReservation(CampaignReservation campaignReservation) {
        this.campaignReservation = campaignReservation;
    }

    public ReservationPackageItem getReservationPackageItem() {
        return reservationPackageItem;
    }

    public void setReservationPackageItem(ReservationPackageItem reservationPackageItem) {
        this.reservationPackageItem = reservationPackageItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationDailyAssetPK != null ? reservationDailyAssetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationDailyAsset)) {
            return false;
        }
        ReservationDailyAsset other = (ReservationDailyAsset) object;
        if ((this.reservationDailyAssetPK == null && other.reservationDailyAssetPK != null) || (this.reservationDailyAssetPK != null && !this.reservationDailyAssetPK.equals(other.reservationDailyAssetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ntuc.adserver.ReservationDailyAsset[ reservationDailyAssetPK=" + reservationDailyAssetPK + " ]";
    }
    
}
