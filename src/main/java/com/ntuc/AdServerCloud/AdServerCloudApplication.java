package com.ntuc.AdServerCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdServerCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdServerCloudApplication.class, args);
	}

}
